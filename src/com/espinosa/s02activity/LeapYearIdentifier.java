package com.espinosa.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a year: ");
        int year = scanner.nextInt();

        if(year % 4 == 0) {
            if(year % 100 == 0) {
                if(year % 400 == 0) {
                    System.out.println("Year " + year + " is a leap year. It has 366 days.");
                } else {
                    System.out.println("Year " + year + " is not a leap year. It has 365 days.");
                }
            } else {
                System.out.println("Year " + year + " is a leap year. It has 366 days.");
            }

        } else {
            System.out.println("Year " + year + " is not a leap year. It has 365 days.");
        }
    }
}
